#!/usr/bin/env python2

#written by B. Leverington leverington@physi.uni-heidelberg.de 31.08.2017

#some code copied from http://www.meteo-blog.net/2012-05/dewpoint-calculation-script-in-python/
#usbtmc.py is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py
#
#execute as $ sudo ~/sw/miniconda2/bin/python readTSP01tsmc.py

import usbtmc
import time
from datetime import datetime
import numpy as np

# dewpoint constants
a = 17.271
b = 237.7 # degC
 
 
def dewpoint_approximation(T,RH):
 
    Td = (b * gamma(T,RH)) / (a - gamma(T,RH))
 
    return Td
 
 
def gamma(T,RH):
 
    g = (a * T / (b + T)) + np.log(RH/100.0)
 
    return g
 
 
def w(command):
    if len(command) < 30: print command
    d.write(command)
    time.sleep(0.2)

def do_something(outfile):
    d.write("MEAS:TEMP1?")
    temp1 = float(d.read());
    d.write("MEAS:TEMP2?")
    temp2 = float(d.read());
    d.write("MEAS:TEMP3?")
    temp3 = float(d.read())
    d.write("MEAS:HUM?")
    humi1 = float(d.read())
    Td = float(dewpoint_approximation(temp1,humi1))
    print('{0} {1:4.2f} {2:4.2f} {3:4.2f} {4:4.2f} {5:4.2f} '.format(time.time(), temp1, temp2, temp3, humi1, Td))
    outfile.write('{0} {1:4.2f} {2:4.2f} {3:4.2f} {4:4.2f} {5:4.2f} '.format(time.time(), temp1, temp2, temp3, humi1, Td))


listOfDevices = usbtmc.getDeviceList()
dn = listOfDevices[0] #assume it is the first device in the list
d = usbtmc.UsbTmcDriver(dn)
print d.getName()
d.write("INIT")

datestring = datetime.strftime(datetime.now(), '%Y-%m-%d')
outfile = open('templog_TSP01_'+datestring+'.txt', 'w')
 
print("Temperature logger: Thorlabs TSP01")
print("Ctrl+C to exit\n")
print("time(UTC) T1(int) T2(ext) T3(ext) RH% Dewpt")

try:
    while True:
        do_something(outfile) #the measurement. see def above
        time.sleep(25.5) #time between measurements+4.5s to return result
except KeyboardInterrupt:
    pass

outfile.close


